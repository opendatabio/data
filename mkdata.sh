#!/bin/bash
# Flags para o MySQLdump
DUMPOPTS="--single-transaction --no-create-info --skip-add-drop-table"
# Informações de conexão
CONNOPTS="-uopendatabio -p opendatabio"
DATE=`date +%Y-%m-%d`
LOCATIONSEED=LocationSeed_BrazilUCsTIs_$DATE.sql
TAXONSEED=TaxonSeed_APWebOrderLevelTree_$DATE.sql

# Gera o dump
mysqldump $DUMPOPTS $CONNOPTS locations --where "adm_level=(-1) OR (lft>=(SELECT lft FROM locations WHERE name='Brasil' AND adm_level=2) AND lft<=(SELECT rgt FROM locations WHERE name='Brasil' AND adm_level=2) AND (adm_level<=8 OR adm_level IN(98,97,99))) OR id IN(SELECT related_id FROM location_related WHERE location_id IN(SELECT id FROM locations WHERE adm_level=(-1) OR (lft>=(SELECT lft FROM locations WHERE name='Brasil' AND adm_level=2) AND lft<=(SELECT rgt FROM locations WHERE name='Brasil' AND adm_level=2) AND (adm_level<=8 OR adm_level IN(98,97,99)))))" > $LOCATIONSEED

mysqldump $DUMPOPTS $CONNOPTS location_related --where "location_id IN(SELECT id FROM locations WHERE adm_level=(-1) OR (lft>=(SELECT lft FROM locations WHERE name='Brasil' AND adm_level=2) AND lft<=(SELECT rgt FROM locations WHERE name='Brasil' AND adm_level=2) AND (adm_level<=8 OR adm_level IN(98,97,99))))" >> $LOCATIONSEED
# Adiciona o comando para remover os locations existentes
sed -i '16a\
SET FOREIGN_KEY_CHECKS=0;\
DELETE FROM locations;\
DELETE FROM location_related;
' $LOCATIONSEED
# Comprime o arquivo
tar czf $LOCATIONSEED.tar.gz $LOCATIONSEED

# Gera o dump
mysqldump $DUMPOPTS $CONNOPTS taxons --where "author_id IS NULL  AND id<=109" > $TAXONSEED
mysqldump $DUMPOPTS $CONNOPTS taxon_external --where "taxon_id IN(SELECT id FROM taxons WHERE author_id IS NULL AND id<=109)" >> $TAXONSEED
mysqldump $DUMPOPTS $CONNOPTS taxon_bibreference --where "taxon_id IN(SELECT id FROM taxons WHERE author_id IS NULL  AND id<=109)" >> $TAXONSEED
# Adiciona o comando para remover os taxons existentes
sed -i '16a\
SET FOREIGN_KEY_CHECKS=0;\
DELETE FROM taxons;\
DELETE FROM taxon_external;\
DELETE FROM taxon_bibreference;
' $TAXONSEED
# Comprime o arquivo
tar czf $TAXONSEED.tar.gz $TAXONSEED
